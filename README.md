## Loopring Arbitrageur

This is a quick script that I am using to quick analyze potential
arbitrage opportunities in the different liquidity pools available at
[Loopring](https://exchange.loopring.io).

The script uses the coingecko and loopring API to query for the market
price of the token and the pool availability. It checks if a pool have
an unbalanced pair and provides an estimate of how much you can profit
by buying the undervalued token.

To use it, you can call it with

```
python loopring.py --token <token_symbol>
```

to check all pools involving a specific token or

```
python loopring.py --pool <token_symbol> <token_symbol>
```

to check a specific pool.

You can pass `--pool` or `--token` multiple times if you want.

This script is *NOT* meant for automated trading or any real-time
operation, and is literally the result of a late night of work and
some curiosity regarding loopring API. Use it at your own discretion.
